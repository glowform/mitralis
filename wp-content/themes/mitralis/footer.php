			<footer class="footer" role="contentinfo">

				<div class="wrap cf">
					<?php get_sidebar(); ?>
				</div>

				<div class="copyright-bar">
					<div class="wrap cf">
						<div class="source-org copyright">
							<ul>
								<li>Copyright 2018</li>
								<li><a href="#">privacy disclaimer</a></li>
								<li>ontwerp & realisatie: <a href="#">new yellow</a></li>
							</ul>
						</div>
					</div>
				</div>

			</footer>

		</div>

		<?php // all js scripts are loaded in library/corise.php ?>
		<?php wp_footer(); ?>

		<script defer type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyAEuN36n-7NWHsVSXy57ZbrG2IDahcJgxM"></script>

	</body>

</html> <!-- end of site. what a ride! -->
