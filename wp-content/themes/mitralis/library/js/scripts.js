/*
 * Corise Scripts File
 *
 * This file should contain any js scripts you want to add to the site.
 * Instead of calling it in the header or throwing it inside wp_head()
 * this file will be called automatically in the footer so as not to
 * slow the page load.
 *
 * There are a lot of example functions and tools in here. If you don't
 * need any of it, just remove it. They are meant to be helpers and are
 * not required. It's your world baby, you can do whatever you want.
*/


/*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
*/
function updateViewportDimensions() {
	var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],x=w.innerWidth||e.clientWidth||g.clientWidth,y=w.innerHeight||e.clientHeight||g.clientHeight;
	return { width:x,height:y }
}
// setting the viewport width
var viewport = updateViewportDimensions();


/*
 * Throttle Resize-triggered Events
 * Wrap your actions in this function to throttle the frequency of firing them off, for better performance, esp. on mobile.
 * ( source: http://stackoverflow.com/questions/2854407/javascript-jquery-window-resize-how-to-fire-after-the-resize-is-completed )
*/
var waitForFinalEvent = (function () {
	var timers = {};
	return function (callback, ms, uniqueId) {
		if (!uniqueId) { uniqueId = "Don't call this twice without a uniqueId"; }
		if (timers[uniqueId]) { clearTimeout (timers[uniqueId]); }
		timers[uniqueId] = setTimeout(callback, ms);
	};
})();

// how long to wait before deciding the resize has stopped, in ms. Around 50-100 should work ok.
var timeToWaitForLast = 100;


/*
 * Here's an example so you can see how we're using the above function
 *
 * This is commented out so it won't work, but you can copy it and
 * remove the comments.
 *
 *
 *
 * If we want to only do it on a certain page, we can setup checks so we do it
 * as efficient as possible.
 *
 * if( typeof is_home === "undefined" ) var is_home = $('body').hasClass('home');
 *
 * This once checks to see if you're on the home page based on the body class
 * We can then use that check to perform actions on the home page only
 *
 * When the window is resized, we perform this function
 * $(window).resize(function () {
 *
 *    // if we're on the home page, we wait the set amount (in function above) then fire the function
 *    if( is_home ) { waitForFinalEvent( function() {
 *
 *      // if we're above or equal to 768 fire this off
 *      if( viewport.width >= 768 ) {
 *        console.log('On home page and window sized to 768 width or more.');
 *      } else {
 *        // otherwise, let's do this instead
 *        console.log('Not on home page, or window sized to less than 768.');
 *      }
 *
 *    }, timeToWaitForLast, "your-function-identifier-string"); }
 * });
 *
 * Pretty cool huh? You can create functions like this to conditionally load
 * content and other stuff dependent on the viewport.
 * Remember that mobile devices and javascript aren't the best of friends.
 * Keep it light and always make sure the larger viewports are doing the heavy lifting.
 *
*/

/*
 * We're going to swap out the gravatars.
 * In the functions.php file, you can see we're not loading the gravatar
 * images on mobile to save bandwidth. Once we hit an acceptable viewport
 * then we can swap out those images since they are located in a data attribute.
*/
function loadGravatars() {
  // set the viewport using the function above
  viewport = updateViewportDimensions();
  // if the viewport is tablet or larger, we load in the gravatars
  if (viewport.width >= 768) {
  jQuery('.comment img[data-gravatar]').each(function(){
    jQuery(this).attr('src',jQuery(this).attr('data-gravatar'));
  });
	}
} // end function


if ( /^((?!chrome|android).)*safari/i.test(navigator.userAgent)) {
  jQuery('.rating-box').addClass('safari-only');

}


/*
 * Put all your regular jQuery in here.
*/
jQuery(document).ready(function($) {

  /*
   * Let's fire off the gravatar function
   * You can remove this if you don't need it
  */
  loadGravatars();

  $('.custom_media_upload').click(function() {

        var send_attachment_bkp = wp.media.editor.send.attachment;
        var button = $(this);

        wp.media.editor.send.attachment = function(props, attachment) {

            $(button).prev().prev().attr('src', attachment.url);
            $(button).prev().val(attachment.url);

            wp.media.editor.send.attachment = send_attachment_bkp;
        }

        wp.media.editor.open(button);

        return false;       
    });

  var menu_opened = false;

  $('.menu-btn').click(function() {
    if (menu_opened == true) {
        $('.header nav').hide();
        menu_opened = false;
    } else {
        $('.header nav').show();
        menu_opened = true;
    }
  });

  var search_opened = false;


  $('.search-btn').click(function() {
    if (search_opened == true) {
        $('.header .wpdreams_asl_container ').hide();
        search_opened = false;
        $(this).removeClass('close');
    } else {
        $('.header .wpdreams_asl_container ').show();
        search_opened = true;
        $(this).addClass('close');
    }
  });

  

  $('.hover-box').each(function() {
    link_attr = $(this).find('.btn-slide').attr('href');
    $(this).wrap('<a href="'+link_attr+'"></a>');
  });

  if (typeof google === 'object' && typeof google.maps === 'object') {
   // When the window has finished loading create our google map below
    google.maps.event.addDomListener(window, 'load', init);

    function init() {
        // Basic options for a simple Google Map
        // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
        var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: 11,

            // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng(40.6700, -73.9400), // New York

            // How you would like to style the map. 
            // This is where you would paste any style found on Snazzy Maps.
            styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"},{"lightness":33}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f7f7f7"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#deecdb"}]},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":"25"}]},{"featureType":"road","elementType":"all","stylers":[{"lightness":"25"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"saturation":"-90"},{"lightness":"25"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"transit.line","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"transit.station","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"visibility":"on"},{"color":"#e0f1f9"}]}]
        };

        // Get the HTML DOM element that will contain your map 
        // We are using a div with id="map" seen below in the <body>
        var mapElement = document.getElementById('map');

        // Create the Google Map using our element and options defined above
        var map = new google.maps.Map(mapElement, mapOptions);

        // Let's also add a marker while we're at it
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(40.6700, -73.9400),
            map: map,
            title: 'Snazzy!'
        });
    }
  }
  
  //FAQS

  //unbind default vc triggers 
  $('.vc_toggle').removeClass('vc_toggle').addClass('faq_toggle cf');
  $('.vc_toggle_title').removeClass('vc_toggle_title').addClass('faq_toggle_title');


  //get answer height at it's full width
  jQuery.fn.getHeight = function(callback){
      var elem;
      this.each(function(i, el){
          width = $('.faq_toggle .faq_toggle_title').width();
          el = jQuery(el), elem = el.clone().css({"height":"auto","width":width}).appendTo(".faq-section .wpb_wrapper");
          f_height = elem.css("height");
          console.log(f_height);
          elem.remove();
      }); 
      return f_height; 
  }

  $('.faq_toggle').append('<div class="arrow ar-wide"></div>')
  $('.faq_toggle').append('<div class="arrow ar-mobile"></div>')


  $('.faq_toggle .ar-wide').toggle(function() {
      $(this).addClass('down');
      $(this).parent().addClass('opened');
      full_height = $(this).siblings('.vc_toggle_content').getHeight();
      console.log(full_height);
      $(this).siblings('.vc_toggle_content').animate({width: '70%', height: full_height});
      $(this).siblings('.faq_toggle_title').animate({width: '30%'});
  },function() {
      $(this).removeClass('down');
      $(this).parent().removeClass('opened');
      $(this).siblings('.faq_toggle_title').animate({width: '70%'});
      $(this).siblings('.vc_toggle_content').animate({width: '30%', height: '0px'});
  });

  //for mobile
  $('.faq_toggle .ar-mobile').toggle(function() {
      $(this).addClass('down');
      $(this).parent().addClass('opened');
      full_height = $(this).siblings('.vc_toggle_content').getHeight();
      $(this).siblings('.vc_toggle_content').animate({ height: full_height});
      
  },function() {
      $(this).removeClass('down');
      $(this).parent().removeClass('opened');
      $(this).siblings('.vc_toggle_content').animate({height: '0px'});
  });


if (typeof $.fn.slick !== 'undefined' && $.isFunction($.fn.slick)) {
  $('.slick').slick({
    centerMode: true,
    centerPadding: '360px',
    slidesToShow: 1,
    infinite: true,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1440,
        settings: {
          centerPadding: '200px',
        }
      },
      {
        breakpoint: 1030,
        settings: {
          centerPadding: '100px',
        }
      },
      {
        breakpoint: 768,
        settings: {
          centerPadding: '40px',
        }
      },
      {
        breakpoint: 481,
        settings: {
          //arrows: false,
          centerMode: false,
          slidesToShow: 1
        }
      }
    ]
  });
}

}); /* end of as page load scripts */
