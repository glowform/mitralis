<?php get_header(); ?>

			<div id="content">
				<?php if(!is_front_page()) { ?>
					<div class="breadcrumbs">
						<ul>
							<?php

							global $post;     // if outside the loop

							if ( is_page() && $post->post_parent ) {
							    echo '<li><a href="#">'. get_the_title($post->post_parent) .'</a></li>';

							} else {
							   echo '<li class="home"><a href="'.get_home_url().'">home</a></li>';
							}
							?>
							<li>
								<a href="#"><?php echo get_the_title(); ?></a>
							</li>
						</ul>
					</div>
				<?php }; ?>

				<div id="inner-content" class="wrap cf">

						<div id="main" class="cf" role="main">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
								<section class="entry-content cf" itemprop="articleBody">
									<?php
										// the content (pretty self explanatory huh)
										the_content();

									?>
								</section> <?php // end article section ?>

							</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry cf">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'corisetheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'corisetheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page.php template.', 'corisetheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</div>

				</div>

			</div>

<?php get_footer(); ?>
