<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

					<div id="main" class="m-all t-2of3 d-5of7 cf" role="main">

						<article id="post-not-found" class="hentry cf">

							<header class="article-header">

								<h2><?php _e( 'Epic 404 - Article Not Found', 'corisetheme' ); ?></h2>

							</header>

							<section class="entry-content">

								<p><?php _e( 'The article you were looking for was not found, but maybe try looking again!', 'corisetheme' ); ?></p>

							</section>

							<footer class="article-footer">

									<!-- <p><?php _e( 'This is the 404.php template.', 'corisetheme' ); ?></p> -->

							</footer>

						</article>

					</div>

				</div>

			</div>

<?php get_footer(); ?>
