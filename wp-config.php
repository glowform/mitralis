<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mitralis');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'C?{F@TR@RjXW,G%I.?)e/76|Zl=K-y!=i=ibOj,5RDv gL^Zfd<5j_gtQC`Qgb;f');
define('SECURE_AUTH_KEY',  'Dt5OgS5y-KT5+c6% TOHQ6kHxk@Y%+I7tB#IW,-#.GC?MmZs:|wZHXlGJ+o8hBW8');
define('LOGGED_IN_KEY',    'dF8f9Wn!9+^FGham=1Sp]^c*NhjH=A/Z 7rXpW`2>t xxpQ7@LJI*]?:60X(mz^h');
define('NONCE_KEY',        '8 n@#z[:SD>*U1l&%-^4WZxM$~yS<`$O 8rKKlkq%ZKH%BzOpZ.zM)R.K4!(Sibk');
define('AUTH_SALT',        '46W`_cQ5R9!{slNU[.>qyC *K7sT^;l73Q48+}K:&LON|:<o,>[~E7Vqtu33X<#~');
define('SECURE_AUTH_SALT', 'W(W;@0l?>d]brk9nak/aU>Nf.[YvfDFB6c])SyB*^}Yz4u~1QAIJcDf27/ud `S{');
define('LOGGED_IN_SALT',   'C^cNC>-m1S,xn&pku||=-.1&#aqwWW@Lf@ ~|I^nFjl8N9V/_`8:}|pAmis)t=TG');
define('NONCE_SALT',       'B}N!VCa-n]luTt>pvL[%SFay[_ym%SVX{X7>ORasQ:`KYBR:,cqun`dg1w%9aLLj');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'mwp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
